<section id="body_part">
    <div class="container">
        <div class="body_image" >
            <img src="/assets/images/banner.jpg" class="img-fluid" style="width: 100%;"> 
        </div>
    </div>
          
    <div class="container">
        <div class="review_section">
            <div class="row">
                <div class="col-sm-8">
                    <div class="review_header">
                        <h2>Customer Review</h2>
                    </div>

                    <?php 
                        foreach($feedbacks as $feedback){
                    ?>
                    <div class="review_one">
                        <div class="comment_header">
                            <div class="comment_header_rating" style="display: inline-block;">
                                <?php if($feedback->rating == 5 ) {?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                <?php }elseif($feedback->rating == 4) {?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                <?php }elseif($feedback->rating == 3) {?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star"></span>
                                <?php }elseif($feedback->rating == 2) {?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star"></span>
                                <?php }else{?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star"></span>
                                <?php } ?>
                            </div>
                            <div class="comment_header_comment" style="display: inline-block;">
                                <h5><?php echo $feedback->subject; ?></h5>
                            </div>
                        </div>
                        <div class="comment_author" style="display: block;">
                            <?php 
                                $str = $feedback->date;
                                $str = strtotime($str);
                                $year = date("Y", $str); 
                                $month = date("F", $str); 
                                $day = date("d", $str); 
                            ?>
                            <h5><strong><?php echo $feedback->courier; ?></strong> used By <?php echo $feedback->customer_name; ?> on <?php echo $month; ?> <?php echo $day; ?>, <?php echo $year; ?>.</h5>  
                        </div>
                        <hr>
                              
                        <div class="comment_body">
                            <h4>
                                <?php echo $feedback->message; ?>
                            </h4>
                        </div>  
                    </div>
                    <hr>
                        <?php } ?>
                    
                    <ul class="pagination">     
                        <?php echo $this->pagination->create_links(); ?>
                    </ul>          
                    <!-- <nav>
                        <ul class="pagination pg-darkgrey">
                            <li class="page-item">
                                <a class="page-link" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                           
                            <li class="page-item active"><a class="page-link">1</a></li>
                            <li class="page-item"><a class="page-link">2</a></li>
                            <li class="page-item"><a class="page-link">3</a></li>
                            <li class="page-item"><a class="page-link">4</a></li>
                            <li class="page-item"><a class="page-link">5</a></li>
                            <li class="page-item">
                                <a class="page-link" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
                      
                <div class="col-sm-4">
                    <div class="courier_ratings" style="margin-top: 40px;">
                          
                    </div>


                    <form>
                        <div class="review_writing" >
                            <div class="review_writing_header">
                                <h3>Write a Review</h3>
                            </div>
                         
                            <div class="review_writing_body">
                                <div class="review_writing_body_rating">
                                    <div class="your_ratings">
                                        <h5>How do you rate our service?</h5>
                                    </div>
                                    <div class="ratings_star">
                                        <div class="rate">
                                            <input type="radio" id="star5" name="rate" value="5" />
                                            <label for="star5" title="">5 stars</label>
                                            <input type="radio" id="star4" name="rate" value="4" />
                                            <label for="star4" title="">4 stars</label>
                                            <input type="radio" id="star3" name="rate" value="3" />
                                            <label for="star3" title="">3 stars</label>
                                            <input type="radio" id="star2" name="rate" value="2" />
                                            <label for="star2" title="">2 stars</label>
                                            <input type="radio" id="star1" name="rate" value="1" />
                                            <label for="star1" title="">1 star</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="detail-form-space1">
                                    <input class="efs-input"  type="text" placeholder="Full Name" name="fullname" id="fullname">
                                </div>

                                <div class="detail-form-space">
                                    <input class="efs-input"  type="text" placeholder="Email Address" name="emailaddress" id="emailaddress">
                                </div>

                                <div class="detail-form-space">
                                    <input class="efs-input"  type="text" placeholder="Phone Number" name="phonenumber" id="phonenumber">
                                </div>
                                <div class="detail-form-space">
                                   <select class="form-control" id="select_courier">
                                    <option value="">Select Courier</option>
                                    <option value="Courier Plus">Courier Plus</option>
                                    <option value="DHL">DHL</option>
                                    <option value="UPS">UPS</option>
                                    <option value="Skynet">Skynet</option>
                                    <option value="Muve">Muve</option>
                                   </select>
                                </div>
                                <div class="review_header">
                                    <h3>Your review</h3>
                                </div>
                                <div class="detail-form-space1">
                                    <input class="efs-input2" type="text1" placeholder="Subject" style="color: #fff;" name="subject" id="subject">
                                </div>
                                <div class="detail-form-space1">
                                    <textarea class="efs-input3" style="height: 300px;" type="text2" placeholder="Message" name="message" id="message"></textarea>
                                </div>
                                <div class="button-area">
                                     <input type="button" id="review_form" class="butttonarea2" value="SUBMIT">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>