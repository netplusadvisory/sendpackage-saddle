<!DOCTYPE html>
<html lang="en">
<head>
	<title>Send Package Successful</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets/vendor/bootstrap/css/bootstrap.min.css">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>
<!--  -->
<div class="simpleslide100">
    <div class="simpleslide100-item bg-img1" style="background-image: 'http://sendpackage.saddle.ng/assets/images/bg03.jpg';"></div>
</div>

<div class="size1 overlay1">
    <!--  -->
    <div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
        <h3 class="l1-txt1 txt-center p-b-25">
            Transaction Successful 
        </h3>
        <div class="col-sm-8 col-sm-offset-2 text" align="center">
            <h1><strong>Thank you for using our service</strong> Package Sent Successfully</h1>
        </div>
        <button class="btn btn-success"><a style="color:white;" href="<?php echo base_url(); ?>">Done</a></button>
    </div>
</div>

</body>
</html>