<?php var_dump($_SESSION); ?>
<section id="body_part" style="background-color:#E0E0E0;">

    <div class="body-part-container">
        <div class="container">
            <div class="containermini">
                <div class="complete_order">
                    <div class="complete_header">
                        <h1>Order Completed!</h1>  
                    </div>
 
                    <div class="complete_body">
                        <h5>We are on our way to pick up your package</h5>  
                    </div>
                </div>
            </div>
        </div>
    
        <form>
            <div class="container">
                <div class="containermini">
                    <div class="row">
     
                        <div class="col-sm-12" >
                            <div class="rhs2">

                                <div class="feedback">
                                    <div class="feedback_header">
                                        <h2>WE'D LOVE YOUR FEEDBACK</h2>
                                    </div>
                                    <div class="feedback_body">
                                        <h5>Please let us know how we're doing by leaving us a review</h5>
                                    </div>
                                    <hr>
                                </div>
    
                                <div class="review_writing_body_rating1">
                                    <div class="your_ratings1" style="display: inline-block;">
                                        <h5>How do you rate our service?</h5>
                                    </div>
                                    <div class="ratings_star1" style="display: inline-block; margin-left: 10px;">
                                        <div class="rate1">
                                            <input type="radio" id="star5" name="rate" value="5" />
                                            <label for="star5" title="">5 stars</label>
                                            <input type="radio" id="star4" name="rate" value="4" />
                                            <label for="star4" title="">4 stars</label>
                                            <input type="radio" id="star3" name="rate" value="3" />
                                            <label for="star3" title="">3 stars</label>
                                            <input type="radio" id="star2" name="rate" value="2" />
                                            <label for="star2" title="">2 stars</label>
                                            <input type="radio" id="star1" name="rate" value="1" />
                                            <label for="star1" title="">1 star</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="review_header1">
                                        <h5>Write a review</h5>
                                    </div>
                                    <input type="hidden" id="name" name="name" value="<?php echo $_SESSION['name']; ?>" />
                                    <input type="hidden" id="phone" name="phone" value="<?php echo $_SESSION['phone_number']; ?>" />
                                    <input type="hidden" id="email" name="email" value="<?php echo $_SESSION['email']; ?>" />
                                    <input type="hidden" id="courier_id" name="courier_id" value="<?php echo $_SESSION['courier_id']; ?>" />
                                    <div class="detail-form-space1">
                                        <input class="efs-input4" action="post" type="text1" placeholder="Subject" style="color: #fff;" name="subject" id="subject">
                                    </div>
                                    
                                    <div class="detail-form-space1">
                                        <textarea class="efs-input5" style="height: 150px;" action="post" type="text2" placeholder="Message" name="message" id="message"></textarea>
                                    </div>
                                    <div class="button-area">
                                        <input type="button" id="feedback_form" class="buttton" value="SUBMIT">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

