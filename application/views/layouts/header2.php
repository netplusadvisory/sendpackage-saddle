<!DOCTYPE html>
<html lang="en">

  <head>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/assets/images/web_icon.jpg"/>
    <title>send a package</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/home.css" rel="stylesheet">
    <link href="/assets/css/5cols.css" rel="stylesheet">
    <link href="/assets/css/col.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <script src="/assets/vendor/jquery/jquery.js"> </script>
    <script type="text/javascript" src="/assets/js/script.js"></script>

  </head>

  <body>
    <section id="header">
    <div class="header-part">
      <div class="container">
      <div class="row" align="right">
           <div class="col-sm-12">
             <div class="menu">
             <ul>
                  <li><a href="<?php echo base_url(); ?>review" style="text-decoration: none; color:white;">Reviews</a></li>
                  <li><a href="tel:0907716019" style="text-decoration: none; color:white;">Call 0907 7716 019</a></li>
                   <a href="https://new.saddleng.com" style="text-decoration:none; color:#fff;" ><li> <img src="/assets/images/logo2.jpg"></li></a>
              </ul>
            </div>
      </div>
      </div>


      </div>
    </div>

  
</section>
