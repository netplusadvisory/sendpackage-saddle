<?php

class Feedback_model extends CI_Model{


    function __construct()
    {
        parent::__construct();
    }

    public function store_feedback($data) 
    {
        $this->db->insert('feedback', $data);
        return $this->db->insert_id();
    } 

    public function record_count() {
 
        return $this->db->count_all("feedback");
  
    }

    public function get_feedbacks($limit, $start)
    {

        $this->db->limit($limit, $start);
        $this->db->select('*');
        $this->db->from('feedback');
        $this->db->order_by('id','DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
 
            foreach ($query->result() as $row) {
  
                $data[] = $row;
  
            }
  
            return $data;
  
        }
  
        return false;
    }

    public function get_courier_nmae($id)
    {
        $this->db->select('courier_name')->from('couriers')
                ->where('courier_id', $id);
        $courier = $this->db->get();
        if($courier->num_rows() > 0) 
        {
            return $courier->row();
        }else{
            return false;
        }
    }

}
?>