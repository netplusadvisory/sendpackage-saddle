<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 ** Developer: Manasseh Abiodun
** Sendpackage Version: 1.0
*/
class Review extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('feedback_model');
        $this->load->helper("url");
        $this->load->library("pagination");
        
	}
	
	public function index()
	{


        $config = array();
 
        $config["base_url"] = base_url() . "review/index";
  
        $config['total_rows'] = $this->feedback_model->record_count();
        $config["per_page"] = 5;
  
        $config['uri_segment'] = 3;
        $config['num_links']= 5;

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>'; 
  
        $this->pagination->initialize($config);
        

        $data['feedbacks'] = $this->feedback_model->get_feedbacks($config['per_page'], $this->uri->segment(3));
        $config['total_rows'] = count($data['feedbacks']);

        //$data['links'] = $this->pagination->create_links();

		$this->load->view('layouts/header2');
		$this->load->view('review', $data);
		$this->load->view('layouts/footer');
    }

    public function save()
    {
        $data['customer_name'] = $this->input->post('name');
        $data['email'] = $this->input->post('emailaddress');
        $data['phone']  = $this->input->post('phonenumber');
        $data['courier']  = $this->input->post('courier');
        $data['subject'] = $this->input->post('subject');
        $data['message'] = $this->input->post('message');
        $data['rating'] = $this->input->post('rate');
        $data['date'] = date('Y-m-d H:i:s');

        $this->feedback_model->store_feedback($data);
 
        redirect('review');
    }

   
}