<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 ** Developer: Manasseh Abiodun
** Sendpackage Version: 1.0
*/
class Feedback extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('feedback_model');
        $this->load->library("pagination");
	}
	
	public function index()
	{
	
		$this->load->view('layouts/header2');
		$this->load->view('feedback');
		$this->load->view('layouts/footer');
    }

    public function save()
    {
        $courier = $this->input->post('courier_id');
     
        $courier = $this->feedback_model->get_courier_nmae($courier);
        
        $feedback['courier'] = $courier->courier_name;
        $feedback['customer_name'] = $this->input->post('name');
        $feedback['phone'] = $this->input->post('phone');
        $feedback['email'] = $this->input->post('email');
        $feedback['subject'] = $this->input->post('subject');
        $feedback['message'] = $this->input->post('message');
       
        $feedback['rating'] = $this->input->post('rate');
        $feedback['date'] = date('Y-m-d H:i:s');

        
        $this->feedback_model->store_feedback($feedback);

        $config = array();
 
        $config["base_url"] = base_url() . "review/index";
  
        $config['total_rows'] = $this->feedback_model->record_count();
        $config["per_page"] = 5;
  
        $config['uri_segment'] = 3;
        $config['num_links']= 5;

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>'; 
  
        $this->pagination->initialize($config);
        

        $data['feedbacks'] = $this->feedback_model->get_feedbacks($config['per_page'], $this->uri->segment(3));
        $config['total_rows'] = count($data['feedbacks']);
        
        redirect('review', $data);
        
        $this->session->sess_destroy();
    }
}