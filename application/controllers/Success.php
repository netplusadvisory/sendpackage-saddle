<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 ** Developer: Manasseh Abiodun
** Sendpackage Version: 1.0
*/
class Success extends CI_Controller {

	function __construct()
    {
        parent::__construct();
	}
	
	public function index()
	{
        $this->load->view('success');
    }
}