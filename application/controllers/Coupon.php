<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 ** Developer: Manasseh Abiodun
** Sendpackage Version: 1.0
*/
class Coupon extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('coupon_model');
	}
	
	public function index()
	{
        $data['password'] = 2;
        $this->load->view('layouts/header');
        $this->load->view('coupon', $data);
        $this->load->view('layouts/footer');
	
    }

    public function save()
    {
        $data['coupon_name'] = $this->input->post('coupon_name');
        $data['coupon_value'] = $this->input->post('coupon_value');
        $password = $this->input->post('password');

        if($password == 'saddle1980'){
            $this->coupon_model->store_coupon($data);
            $data['password'] = 0;
            $this->load->view('layouts/header');
            $this->load->view('coupon', $data);
            $this->load->view('layouts/footer');
        }else{
            $data['password'] = 1;
            $this->load->view('layouts/header');
            $this->load->view('coupon', $data);
            $this->load->view('layouts/footer');
        }
    }

   
}